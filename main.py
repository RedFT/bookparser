### main.py -- the main file of the library project import sys
import subprocess
import sqlite3
import sys

def make_ascii(a_list):
    new_list = []
    for item in a_list:
        new_list.append(str(item))
    return new_list

def get_books(cursor, author="", call_number=""):
    command = "select name from books"
    if call_number != "":
        command += " where call_number=\"" + call_number + "\""

    if author != "":
        command += " where author=\"" + author + "\""
    command += ";"
    cursor.execute(command);
    new_list = []
    for var in cursor.fetchall():
        new_list.append(var[0])
    return make_ascii(new_list)

def get_authors(cursor, book="", call_number=""):
    command = "select call_number from books"
    if book != "":
        command += " where name=\"" + book + "\""

    if call_number != "":
        command += " where call_number=\"" + call_number + "\""
    command += ";"
    cursor.execute(command);
    new_list = []
    for var in cursor.fetchall():
        new_list.append(var[0])
    return make_ascii(new_list)

def get_call_number(cursor, book="", author=""):
    command = "select call_number from books"
    if book != "":
        command += " where name=\"" + book + "\""

    if author != "":
        command += " where author=\"" + author + "\""
    command += ";"
    cursor.execute(command);
    new_list = []
    for var in cursor.fetchall():
        new_list.append(var[0])
    return make_ascii(new_list)


def get_bigrams(word):
    return zip(word, word[1:])


def fs_dice(word, to_match):
    word_bi_g       = get_bigrams(word.upper())
    to_match_bi_g   = get_bigrams(to_match.upper())
    print "\t", word_bi_g
    print "\t", to_match_bi_g

    intersections = [n for n in word_bi_g if n in to_match_bi_g]
    print "\tIntersections: " + str(len(intersections))
    coefficient = (2. * len(intersections)) / (len(word_bi_g) + len(to_match_bi_g))
    return coefficient

    #print word_bi_g
    #print to_match_bi_g
    #print "Intersections " + str(len(intersections))
    #print "Grade: " + str(coefficient)



def get_best_fit(target, search_area):
    highest = 0;
    best_fit = ""
    for area in search_area:
        print target + " vs. " + area
        coefficient = fs_dice(target, area)
        print"\tconfidence: " + str(coefficient)
        print
        if coefficient > highest:
            best_fit = area 
            highest = coefficient
    return best_fit


if __name__ == "__main__":
    bookname = sys.argv[1]
    rotbook  = "rotated_" + bookname

    print "Recognizing Text in Picture " + bookname
    subprocess.call(["convert", "-rotate", sys.argv[2], bookname, rotbook])
    string_to_check = subprocess.check_output(["tesseract", rotbook, "stdout"])
    string_to_check = string_to_check.strip()
    string_to_check = string_to_check.replace("\n","")
    conn = sqlite3.connect("books.db")
    cursor = conn.cursor()
    book_list = get_books(cursor)
    best_name = get_best_fit(string_to_check, book_list)
    
    call_list  = get_call_number(cursor, best_name)
    if call_list:
        print "The best call number for: " + best_name + " is... " + call_list[0]
