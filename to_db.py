import sqlite3
import sys, os

author_file       = "authors.txt"
book_names_file   = "book_names.txt"
call_numbers_file = "call_numbers.txt"

book_names_list   = []
call_numbers_list = []
authors_list      = []

with open(book_names_file, "r") as f:
    n_list = f.readlines()
    for name in n_list:
        book_names_list.append(name.rstrip("\n"))

with open(call_numbers_file, "r") as f:
    n_list = f.readlines()
    for name in n_list:
        call_numbers_list.append(name.rstrip("\n"))

with open(author_file, "r") as f:
    n_list = f.readlines()
    for name in n_list:
        authors_list.append(name.rstrip("\n"))

conn = sqlite3.connect('books.db')
c = conn.cursor()
for index, name in enumerate(book_names_list):
    sqlite_command = 'INSERT INTO books VALUES(' 
    if name != "":
        sqlite_command += ('"' + name + '"')
    else:
        sqlite_command += ('"NULL"')

    sqlite_command += ","

    if call_numbers_list[index] != "":
        sqlite_command += ('"' + call_numbers_list[index] + '"')
    else:
        sqlite_command += ('"NULL"')

    sqlite_command += ","

    if authors_list[index] != "":
        sqlite_command += ('"' + authors_list[index] + '"')
    else:
        sqlite_command += ('"NULL"')

    sqlite_command += ");"

    #print sqlite_command
    c.execute(sqlite_command)

conn.commit()
conn.close()
